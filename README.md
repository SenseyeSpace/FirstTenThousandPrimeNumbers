# First ten thousand prime number

##### Example
```golang
FindNthPrime(101)           -> 547
FindNthPrime(1001)          -> 7927
FindNthPrime(10001)         -> 104743
FindNthPrime(100001)        -> 1299721
FindNthPrime(1000001)       -> 15485867
FindNthPrime(10000001)      -> 179424691
```