package primefinder

import (
	"math"
)

const max = math.MaxUint32 / 2

func FindNthPrime(n uint) uint {
	switch n {
	case 0, 1, 2, 3:
		return n
	}

	processing := make([]uint, 0, n)
	processing = append(processing, 2, 3)

	return search(processing, n)
}

func search(processing []uint, n uint) uint {
	length := uint(len(processing))
	last := processing[length-1]

	for next := last + 2; next < max; next += 2 {
		if isPrime(processing, uint(math.Sqrt(float64(next))), next) {
			processing = append(processing, next)

			length++

			if length == n {
				return processing[length-1]
			}
		}
	}

	return 0
}

func isPrime(processing []uint, sqrt, next uint) bool {
	length := len(processing)

	for i := 0; i < length; i++ {
		prime := processing[i]

		if prime > sqrt {
			return true
		}

		if next%prime == 0 {
			return false
		}
	}

	return true
}
