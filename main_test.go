package primefinder

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindNthPrime(t *testing.T) {
	assert.Equal(t, uint(0), FindNthPrime(0))
	assert.Equal(t, uint(1), FindNthPrime(1))
	assert.Equal(t, uint(2), FindNthPrime(2))
	assert.Equal(t, uint(3), FindNthPrime(3))

	assert.Equal(t, uint(7), FindNthPrime(4))
	assert.Equal(t, uint(11), FindNthPrime(5))
	assert.Equal(t, uint(13), FindNthPrime(6))
	assert.Equal(t, uint(17), FindNthPrime(7))
	assert.Equal(t, uint(19), FindNthPrime(8))

	assert.Equal(t, uint(541), FindNthPrime(100))
	assert.Equal(t, uint(7919), FindNthPrime(1000))
}

func Benchmark101(b *testing.B) {
	benchmarkFindNthPrime(b, 101)
}

func Benchmark10001(b *testing.B) {
	benchmarkFindNthPrime(b, 10001)
}

func benchmarkFindNthPrime(b *testing.B, n uint) {
	for i := 0; i < b.N; i++ {
		FindNthPrime(n)
	}
}
